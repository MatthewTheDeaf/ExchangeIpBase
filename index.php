<?php
include('functions/curlGet.php');

session_set_cookie_params(0);
session_start();
//var_dump($_SESSION);

$obj = curlGet('IpListAll');
?>

<!DOCTYPE HTML>
<html>
<head>
    <title>Айпишники из 1С</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="font-awesome\css\font-awesome.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css"/>
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>

    <!-- Линки, чтобы работал navbar -->
    <link rel="canonical" href="https://getbootstrap.com/docs/4.1/components/navbar/">
    <link href="https://cdn.jsdelivr.net/npm/docsearch.js@2/dist/cdn/docsearch.min.css" rel="stylesheet">

    <!-- Documentation extras -->
    <link href="https://cdn.jsdelivr.net/npm/docsearch.js@2/dist/cdn/docsearch.min.css" rel="stylesheet">
<!--    <link href="/docs/4.1/assets/css/docs.min.css" rel="stylesheet">-->
</head>

<body>


<?php

//Получение файла JSON используя curl
//include('functions/curlGet.php');

//$obj = curlGet('IpListAll');
$objBuilding = curlGet('buildingListAll');
$objDevices = curlGet('deviceListAll');


//$rows = count($obj); // количество строк, tr
//$cols = 2; // количество столбцов, td

//<div class="custom">

?>

<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">Айпишники</a>
        <div class="collapse navbar-collapse" id="navbarColor02">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 pull-right"></ul>
            <!--               <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">-->
            <?php
            if ($obj != 0) {
                echo '<button class="btn btn-outline-light button-nav" type="button" name="downloadExcell" id="downloadExcell">Скачать</button>';
                echo '<form action="/functions/exit.php" method="post">
                        <button class="btn btn-outline-light button-nav" type="submit" name="exitSession" id="exit">Выйти</button>
                      </form>';
            } else {
                echo '<button class="btn btn-outline-light button-nav" type="button" data-bs-toggle="modal" data-bs-target="#exampleModal">Войти</button>';
            }
            ?>

        </div>
    </div>
</nav>

<div class="container-fluid">

    <!--
   <input class="form-control" type="text" placeholder="Поиск..." id="search-text" onkeyup="tableSearch()">
   -->
    <div class="input-group mb-3">
        <span class="input-group-text" for="selectBuilding">
        <i class="fa fa-search fa-2x" for="search-text"></i>
        </span>
        <input class="form-control" type="text" placeholder="Поиск..." id="search-text" onkeyup="tableSearch()">
    </div>

    <div class="row">
        <div class="col">
            <div class="input-group mb-3">
                <label class="input-group-text" for="selectBuilding">Корпус</label>
                <select class="form-select" id="selectBuilding">
                    <option value="">Выберите корпус...</option>
                    <?php
                    for ($i = 0; $i < count($objBuilding); $i++) {
                        echo '<option value="' . $objBuilding[$i]['Code'] . '">' . $objBuilding[$i]['Code'] . '</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="col">
            <div class="input-group mb-3">
                <label class="input-group-text" for="selectDevice">Устройство</label>
                <select class="form-select" id="selectDevice">
                    <option value="">Выберите устройство...</option>
                    <?php
                    foreach ($objDevices as $i => $objDevice) {
                        echo '<option value="' . $objDevice['Code'] . '">' . $objDevice['Code'] . '</option>';
                    }
                    ?>

                </select>
            </div>
        </div>
    </div>


    <!-- table table-hover  - бутстраповские классы -->
    <table class="table-bordered" width="100%" border="1" cellpadding="4" id="info-table">

        <!--
        https://adrianroselli.com/2020/01/fixed-table-headers.html
        https://codepen.io/aardrian/pen/vMpayz
        -->


        <thead>
        <tr>
            <th style="text-align: center;">Айпишник, МАС</th>
            <th style="text-align: center;">Местоположение</th>
            <th style="text-align: center;">Устройство, модель, инвентарник</th>
            <th style="text-align: center;">Телефон</th>
            <th style="text-align: center;">Комментарий</th>
        </tr>
        </thead>
        <tbody id="tbodyTable">


        <?php

        if ($obj != 0) {

            //for ($i = 0; $i < count($obj); $i++) {
            foreach ($obj as $i => $item) {

                $ipMac = $item['Code'];
                if ($item['Mac'] != NULL) $ipMac .= '<br> ' . $item['Mac'];

                $device = $item['Device'];
                if ($item['Model'] != NULL) $device .= ', ' . $item['Model'];
                if ($item['Inv'] != NULL) $device .= '<br> инв: ' . $item['Inv'];


                $phoneTd = $item['Phone'];
                if ($item['Phone'] != NULL && $item['PhoneDlc'] != NULL) {
                    $phoneTd = $item['Phone'] . '<br>доп: ' . $item['PhoneDlc'];
                }
                if ($item['Phone'] == NULL && $item['PhoneDlc'] != NULL) {
                    $phoneTd = 'доп: ' . $item['PhoneDlc'];
                }

                echo '<tr>';
                //echo '<td>' . $item['Code'] . ',<hr> ' . $item['Mac'] . '</td>';
                echo '<td>' . $ipMac . '</td>';
                echo '<td>' . $item['Building'] . ', ' . $item['Cabinet'] . '</td>';
                //echo '<td>' . $item['Device'] . ', ' . $item['Model'] . ', инв:' . $item['Inv'] . '</td>';
                echo '<td>' . $device . '</td>';
                //echo '<td>' . $item['Phone'] . ', доп:' . $item['PhoneDlc'] . '</td>';
                echo '<td>' . $phoneTd . '</td>';
                echo '<td>' . $item['Comment'] . '</td>';
                echo '</tr>';
            }
        }
        ?>
        </tbody>
    </table>

    <?php

    if ($obj == 0) {
        //echo '<div class="four" data-bs-toggle="modal" data-bs-target="#exampleModal"><h1>Авторизуйтесь</h1></div>';
        echo '<div class="logo-title" data-bs-toggle="modal" data-bs-target="#exampleModal">
                <a href="#">Нажми, чтобы<br>Авторизоваться</a>
              </div>';

        //include('forms/login.php'); // Если написать в начале, навбар пепяжит.

    }
    ?>
<!--    <div class="menu">-->
<!--    <button id="scrollToTop" onclick="goUp();">ВВЕРХ </button>-->
<!--    </div>-->
<!--    <div class="scrollup" style="display: block;"></div>-->
<!--    <i class="fa fa-arrow-up" aria-hidden="true"></i>-->
    <button class="scrollup" style="display: block;" id="scrollUp"><i class="fa fa-arrow-up fa-2x" aria-hidden="true"></i></button>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Авторизация</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <?php include('forms/login.php'); // Если написать в начале, навбар пепяжит. ?>
            </div>

        </div>
    </div>
</div>


<!-- http://php-zametki.ru/javascript-laboratoriya/130-javascript-filter-table.html
-->
<script src="js/filterTable.v1.0.min.js"></script>
<script>
    filterTable(document.getElementById("tbodyTable"), {


            /* Фильтр для второго столбца Постепенный ввод слова: */
            1: new filterTable.Filter(document.getElementById('selectBuilding'),
                /* Коллбэк ф-ция валидации */
                function (value, filters, i) {
                    return value.indexOf(filters[i].value) === 0;
                },
                /* Будем вызывать валидацию по событию onkeyup фильтра */
                "onchange"
            ),

            2: new filterTable.Filter(document.getElementById('selectDevice'),
                /* Коллбэк ф-ция валидации */
                function (value, filters, i) {
                    return value.indexOf(filters[i].value) === 0;
                },
                /* Будем вызывать валидацию по событию onkeyup фильтра (поменял на onchange) */
                "onchange"
            )


        }
    );

// Кнопка прокрутки вверх начало
    // https://qna.habr.com/q/482109
    let btn = document.querySelector('.scrollup')
    //
    function magic() {
        if (window.pageYOffset > 20) {
            btn.style.opacity = '1'
        } else { btn.style.opacity = '0' }
    }
    //
    // btn.onclick = function () {
    //     window.scrollTo(0,0)
    // }
    btn.addEventListener('click', () => window.scrollTo({
        top: 0,
        behavior: "smooth", // Почему-то не работает
    }));

    // When scrolling, we run the function
    window.onscroll = magic
    // Кнопка прокрутки вверх конец


</script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
</body>
</html>
