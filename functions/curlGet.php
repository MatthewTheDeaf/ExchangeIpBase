<?php
require './config.php';

function curlGet($action)
{
    //Авторизация
//    session_start();
    if (isset($_SESSION) && isset($_SESSION['login']) && isset($_SESSION['password'])) {
        $username = $_SESSION['login'];
        $password = $_SESSION['password'];
    } else {
        return 'Авторизация не прошла';

    }

    $hostUrlIp = $hostUrlIpTemplate . $action;

    $ch = curl_init($host_api);

// Заполняем параметры CURL для получения данных по запросу GET
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
    curl_setopt($ch, CURLOPT_URL, $hostUrlIp);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);


// Выполнение запроса и получение ответа
    $output = curl_exec($ch);

// Проверка наличия ошибок
    if ($output === FALSE) {
        return 0;
        //echo "cURL Error: " . curl_error($ch);
    } else {

//Выводим сервисную информацию по выполнению запроса
        $info = curl_getinfo($ch);

//echo 'Took ' . $info['total_time'] . ' seconds for url ' . $info['url'];

// Очистка ресурсов
        curl_close($ch);


        //$obj = json_decode($output, true);
        return json_decode($output, true);
    }

}