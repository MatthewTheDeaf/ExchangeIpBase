<!--<script type="text/javascript" src="js/jquery.js"></script>-->

<form action="/functions/authorization.php" method="post" name="authForm" onsubmit="return validateForm()">
    <div class="mb-3">
        <!--                        <label for="recipient-name" class="col-form-label">Логин:</label>-->
        <input type="text" class="form-control" id="input-login" name="login" placeholder="Логин">
    </div>
    <div class="mb-3">
        <input type="password" class="form-control" id="input-password" name="password" placeholder="Пароль">
    </div>
    <div class="mb-3">
        <button type="submit" class="btn btn-primary">Войти</button>
    </div>
    <!--    <div class="alert alert-danger" role="alert" id="message">        -->
    <!--    </div>-->
    <!--    <a id="message" style="color: red; font-size:30px;"></a>-->
</form>

<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
    <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
        <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
    </symbol>
    <symbol id="info-fill" fill="currentColor" viewBox="0 0 16 16">
        <path d="M8 16A8 8 0 1 0 8 0a8 8 0 0 0 0 16zm.93-9.412-1 4.705c-.07.34.029.533.304.533.194 0 .487-.07.686-.246l-.088.416c-.287.346-.92.598-1.465.598-.703 0-1.002-.422-.808-1.319l.738-3.468c.064-.293.006-.399-.287-.47l-.451-.081.082-.381 2.29-.287zM8 5.5a1 1 0 1 1 0-2 1 1 0 0 1 0 2z"/>
    </symbol>
    <symbol id="exclamation-triangle-fill" fill="currentColor" viewBox="0 0 16 16">
        <path d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5c.535 0 .954.462.9.995l-.35 3.507a.552.552 0 0 1-1.1 0L7.1 5.995A.905.905 0 0 1 8 5zm.002 6a1 1 0 1 1 0 2 1 1 0 0 1 0-2z"/>
    </symbol>
</svg>
<div id="message">
</div>


<script>
    // function hide (elements) {
    //     elements = elements.length ? elements : [elements];
    //     for (var index = 0; index < elements.length; index++) {
    //         elements[index].style.display = 'none';
    //     }
    // }

    //hide(document.getElementById('message'));

    function validateForm() {
        let message = '<div class="alert alert-danger d-flex align-items-center alert-dismissible" role="alert"> ' +
            '<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:">' +
            '<use xlink:href="#exclamation-triangle-fill"/>' +
            '</svg>'
            + '<div>'
            + '<strong>Заполните все поля!</strong>'
            + '</div>'
            + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
            + '</div>';

        let wrongAuth = '<div class="alert alert-danger d-flex align-items-center alert-dismissible" role="alert"> ' +
            '<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:">' +
            '<use xlink:href="#exclamation-triangle-fill"/>' +
            '</svg>'
            + '<div>'
            + '<strong>Неверные логин/пароль!</strong>'
            + '</div>'
            + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
            + '</div>';

        let rightAuth = '<div class="alert alert-danger d-flex align-items-center alert-dismissible" role="alert"> ' +
            '<svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Danger:">' +
            '<use xlink:href="#exclamation-triangle-fill"/>' +
            '</svg>'
            + '<div>'
            + '<strong>Верные логин/пароль!</strong>'
            + '</div>'
            + '<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>'
            + '</div>';

        let login = document.forms["authForm"]["login"].value;
        let password = document.forms["authForm"]["password"].value;
        if (login == "" || password == "") {
            document.getElementById("message").innerHTML = message;
            //alert("Заполните все поля");
            return false;
        }
        if (login != "" && password != "") {


            var GLOBAL_INFO;

            // https://kamil-abzalov.ru/videouroki/javascript/javascript-intro-to-ajax-xmlhttprequest/
            var xhr = new XMLHttpRequest();
            xhr.open("GET", 'functions/checkAuth.php?login=' + login + '&password=' + password, false); // `false` makes the request synchronous

            xhr.onreadystatechange = function () {
                if (xhr.readyState != 4) return;
                if (xhr.status != 200) {
                    console.log(xhr.status + ': ' + xhr.statusText); // пример вывода: 404: Not Found
                } else {
                    //alert(xhr.responseText); // responseText -- текст ответа.
                    // var res = JSON.parse(xhr.responseText);
                    // console.log(res);
                    GLOBAL_INFO = xhr.responseText;
                    if (xhr.responseText == 'true') {
                        //document.getElementById("message").innerHTML = rightAuth;
                    } else {
                        document.getElementById("message").innerHTML = wrongAuth;
                        // alert(xhr.responseText); // responseText -- текст ответа.
                    }
                }
            }
            xhr.send();

            //alert(GLOBAL_INFO);
            if (GLOBAL_INFO == 'true'){return true;}
            else {return false;}

        }
    }



</script>



