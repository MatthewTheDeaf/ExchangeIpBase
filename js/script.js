// http://php-zametki.ru/javascript-laboratoriya/130-javascript-filter-table.html
function tableSearch() {
    var phrase = document.getElementById('search-text');
    var table = document.getElementById('info-table');
    var regPhrase = new RegExp(phrase.value, 'i');
    console.log(regPhrase);
    var flag = false;
    for (var i = 1; i < table.rows.length; i++) {
        flag = false;
        for (var j = table.rows[i].cells.length - 1; j >= 0; j--) {
            flag = regPhrase.test(table.rows[i].cells[j].innerHTML);
            if (flag) break;
        }
        if (flag) {
            table.rows[i].style.display = "";
        } else {
            table.rows[i].style.display = "none";
        }

    }
}

function isHidden(el) {
    var style = window.getComputedStyle(el);
    return ((style.display === 'none') || (style.visibility === 'hidden'))
}

function downloadExcell() {

    var data = [], // Объявили пустой массив, чтобы в него запихать ячейки
        tableRow = document.querySelectorAll('#info-table tr'); //Выбрали все строки из таблицы #info-table

    tableRow.forEach(function (row, keyRow) { // Перебираем строки таблицы
        if (isHidden(row)) {
            return;
        }
        let cells = row.querySelectorAll('td'); //Из перебираемых строк выдергиваем ячейки
        // console.log(header);
        if (keyRow === 0) {
            cells = row.querySelectorAll('th');
        }
        if (cells.length != 0) { // Если массив ячеек не пустой
            let arr = [];
            cells.forEach(function (cell, keyCell) { //Для каждой ячейки строки
                arr.push(cell.innerText);
                }
            )
            data.push(arr); // Помещаем в массив [Индекс строки][Индекс ячейки]
        }
    });
    //console.log(data);
    // https://coderoad.ru/47134698/%D0%9F%D0%BE%D0%BB%D1%83%D1%87%D0%B8%D1%82%D1%8C-%D1%84%D0%B0%D0%B9%D0%BB-excel-xlsx-%D0%B8%D0%B7-%D0%BE%D1%82%D0%B2%D0%B5%D1%82%D0%B0-%D1%81%D0%B5%D1%80%D0%B2%D0%B5%D1%80%D0%B0-%D0%B2-ajax
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {

            var contentType = 'application/vnd.ms-excel';
            var filename = "";
            var disposition = this.getResponseHeader('Content-Disposition');
            console.log('disposition = ' + disposition);
            if (disposition && disposition.indexOf('attachment') !== -1) {
                var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/; // Регулярные выражения - гуглить. Маска
                var matches = filenameRegex.exec(disposition);
                if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
            }
            console.log("FILENAME: " + filename);

            try {
                var blob = new Blob([this.response], { type: contentType });

                var downloadUrl = URL.createObjectURL(blob);
                var link = document.createElement("a"); // Данной строкой создаем тег "а" с помощью JS.
                link.href = downloadUrl;
                link.download = filename;
                document.body.appendChild(link);
                link.click();
                link.remove();

            } catch (exc) {
                console.log("Save Blob method failed with the following exception.");
                console.log(exc);
            }
        }
    };
    xhttp.open("POST", "/export/download-excel.php", true);
    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.responseType = 'blob';
    xhttp.send('tableData=' + JSON.stringify(data));
    return false;
}


// Добавляет обработчика событий для кнопки скачать
var el = document.getElementById('downloadExcell');

el.addEventListener("click", downloadExcell, false);

